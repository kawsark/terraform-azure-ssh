provider "azurerm" {
  features {
  }
}

resource "tls_private_key" "pem" {
  algorithm   = "RSA"
  rsa_bits = "4096"
}

resource "azurerm_resource_group" "key_rg" {
  name     = "${var.prefix}-${var.resource_group_name}"
  location = var.location
}

resource "azurerm_ssh_public_key" "azure_private_key" {
  name                = "${var.prefix}-${var.key_name}"
  resource_group_name = azurerm_resource_group.key_rg.name
  location            = azurerm_resource_group.key_rg.location
  public_key          = tls_private_key.pem.public_key_openssh
}
