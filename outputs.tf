output "key_name" {
  value = azurerm_ssh_public_key.azure_private_key.name
  description = "The name of the SSH key that is created"
}

output "key_location" {
  value = azurerm_ssh_public_key.azure_private_key.location
  description = "The region where these resources are created"
}

output "resource_group_name" {
  value = azurerm_resource_group.key_rg.name
  description = "The name of the resource group that is created"
}

output "private_key" {
  value = tls_private_key.pem.private_key_pem
  description = "The private key for logging onto the server"
  sensitive   = true
}

output "public_key" {
  value = tls_private_key.pem.public_key_openssh
  description = "The public key for logging onto the server"
}

