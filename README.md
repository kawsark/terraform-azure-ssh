# terraform-azure-ssh

Terraform configuration to create a SSH keypair in Azure. It can then be used as SSH key public key for VMs

## Steps
- Set Azure provider credentials as ARM_* environment variables
- Optionally set Terraform variables in [variables.tf](variables.tf)
- Run terraform plan and apply
```bash
terraform plan
terraform apply
```
- Obtain ssh key
(Please see example commands in get_key.sh script for saving the private key)
```bash
# Save private key
rm -f ./private_key.pem
terraform output private_key > ./private_key.pem && chmod 400 ./private_key.pem

# Export ip address and connect to target VM via ssh
# export ip=<public-ip>
ssh -i ./private_key.pem ubuntu@${ip}
```
