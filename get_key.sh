#!/bin/bash

# Given the state file url, this script downloads the state file and saves the private key
[[ -z ${url} ]] && echo "Please export the state file download URL: export url=<state_file_url>" && exit 1

wget -O terraform.tfstate ${url}

rm -f ./private_key.pem

terraform output -raw private_key > ./private_key.pem && chmod 400 ./private_key.pem

echo "Saved the private key to ./private_key.pem, use the command below to export full path"
echo "export pem=$(pwd)/private_key.pem"
