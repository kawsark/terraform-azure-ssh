variable "location" {
    description = "The Azure region where the key pair should be created in"
    default = "Central US"

}

variable "prefix" {
    description = "A prefix that is added to resource group and key name"
    default = "demouser-dev"
}

variable "resource_group_name" {
    description = "Name of the resource group in Azure. It will be prefixed with the prefix variable value"
    default = "ssh_rg"
}


variable "key_name" {
    description = "Name of the SSH key in Azure. It will be prefixed with the prefix variable value"
    default = "ssh_key"
}
